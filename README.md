## 2024 - 1
Este es el repositorio del grupo 19, se utilizará le proyecto del paralelo 200 del semestre pasado, los integrantes son:
+ Benjamín López - 202173533-K
+ Felipe León - 202173598-4
+ Eduardo Mondaca - 202173506-2
+ Fernando Salgado - 202173580-1
+ Tutor: Martín Campos

## WIKI
Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.inf.utfsm.cl/sebastian.sepulvedaf/grupo-1-proyecto-inf-236/-/wikis/home)

## Repositorio

[Repositorio Proyecto Final](https://github.com/SebalticaZz/Analisis236)

## 2023 - 2
Este es el repositorio del Grupo 1, cuyos integrantes son:

+ Sebastián Sepulveda Flores - 202104508-2
+ Fernando Salgado Méndez - 202173580-1
+ Tutor: Felipe Marquéz

## Videos

[video avance 1](https://youtu.be/_EE66utw0PY)

[video avance 4](https://youtu.be/Dw6rOS_ZeVY)

[video avance 6-7](https://youtu.be/XGj0daeWLnQ)

## Video Ing. Software

[Video explicativo hito 4](https://www.youtube.com/watch?v=CuuIAwI_xfg)
