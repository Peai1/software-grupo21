import unittest
import requests
import json

class TestGetSolicitud(unittest.TestCase):
    id_solicitud_string = None
    id_solicitud_no_existente = None

    @classmethod
    def setUpClass(cls):
        cls.base_url = "http://localhost:8080"
        cls.id_solicitud_string = "a"
        cls.id_solicitud_no_existente = 8 

    def test_get_solicitud_string(self):
        response = requests.get(f"{self.base_url}/solicitudes/{self.id_solicitud_string}")
        print(response.json())
        print(f"Código de estado esperado: 400, Código de estado obtenido: {response.status_code}")
        self.assertEqual(response.status_code, 400)

    def test_get_solicitud_no_existente(self):
        response = requests.get(f"{self.base_url}/solicitudes/{self.id_solicitud_no_existente}")
        print(response.json())
        print(f"Código de estado esperado: 404, Código de estado obtenido: {response.status_code}")
        self.assertEqual(response.status_code, 404)

    @classmethod
    def tearDownClass(cls):
        del cls.id_solicitud_no_existente
        del cls.id_solicitud_string
        del cls.base_url

if __name__ == "__main__":
    unittest.main()