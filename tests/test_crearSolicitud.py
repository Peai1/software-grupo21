import unittest
import requests
import json

class CreacionSolicitud(unittest.TestCase):
    valid_request_body = None
    invalid_request_body = None
    valid_id_solicitud = None

    @classmethod
    def setUpClass(cls):
        cls.base_url = "http://localhost:8080/crearSolicitud"

        cls.valid_request_body = {
            "rut_solicitante": "12345678-9",
            "numero_cuotas": 12,
            "nombre_cliente": "Juan Perez",
            "direccion_cliente": "123 Main St",
            "valor_uf_actual": "28000",
            "estado": "pendiente",
            "monto_credito": "1000000",
            "derivada": "false",
            "cuota_en_uf": "50000",
            "total_uf": "600000",
            "cuota_clp": "35000",
            "total_clp": "420000"
        }

        cls.invalid_request_body = {
            "rut_solicitante": "12345678-9",
            "numero_cuotas": "doce",
            "nombre_cliente": "Juan Perez",
            "direccion_cliente": "123 Main St",
            "valor_uf_actual": "28000",
            "estado": "pendiente",
            "monto_credito": "1000000",
            "derivada": "false",
            "cuota_en_uf": "50000",
            "total_uf": "600000",
            "cuota_clp": "35000",
            "total_clp": "420000"
        }

        response = requests.post(cls.base_url, json=cls.valid_request_body)
        cls.valid_id_solicitud = response.json()["id_solicitud"]
        cls.codigo_status = response.status_code
        cls.json_response = response.json()
    
    @classmethod
    def tearDownClass(cls):
        requests.delete(f"http://localhost:8080/solicitudes/{cls.valid_id_solicitud}")
        del cls.valid_id_solicitud
        del cls.valid_request_body
        del cls.invalid_request_body
        del cls.base_url
        del cls.codigo_status
        del cls.json_response
    
    def test_crear_solicitud_valida(self):
        print(f"Código de estado esperado: 201, Código de estado obtenido: {self.codigo_status}")
        print(self.json_response)
        self.assertEqual(self.codigo_status, 201)

    def test_crear_solicitud_invalida(self):
        response = requests.post(self.base_url, json=self.invalid_request_body)
        print(response.json())
        print(f"Código de estado esperado: 400, Código de estado obtenido: {response.status_code}")
        self.assertEqual(response.status_code, 400)

if __name__ == "__main__":
    unittest.main()
