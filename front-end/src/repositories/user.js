import axios from "axios";


const updateUser = async (id, data) =>
	axios
		.put(`http://127.0.0.1:8080/users/${id}`, data);

const createUser = async (data) =>
	axios
		.post(`http://127.0.0.1:8080/createUser`, data);

const deleteUser = async (id) =>
	axios.delete(`http://127.0.0.1:8080/users/${id}`).then(res => res.data);

const getAllUsers = () =>
	axios
		.get(`http://127.0.0.1:8080/users`)
		.then((res) => res.data);

const getUser = (id) =>
	axios
		.get(`http://127.0.0.1:8080/users/${id}`)
		.then((res) => res.data);

// eslint-disable-nextline
export { deleteUser, updateUser, createUser, getAllUsers, getUser };
