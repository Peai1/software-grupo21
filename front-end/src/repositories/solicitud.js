import axios from "axios";


const getAllsolicitudes = () =>
	axios
		.get(`http://127.0.0.1:8080/Solicitudes`)
		.then((res) => res.data);


const getSolicitud = (id_solicitud) =>
    axios
        .get(`http://127.0.0.1:8080/solicitudes/${id_solicitud}`)
        .then((res) => res.data);


const Derivar = async (id_solicitud) =>
    axios
        .put(`http://127.0.0.1:8080/Solicitudes/${id_solicitud}/derivar`, id_solicitud);



const Aprobar = async (id_solicitud) =>
    axios
        .put(`http://127.0.0.1:8080/Solicitudes/${id_solicitud}/aprobar`,id_solicitud);



const Rechazar = async (id_solicitud) =>
    axios
        .put(`http://127.0.0.1:8080/Solicitudes/${id_solicitud}/rechazar`, id_solicitud);
        

export { getSolicitud, getAllsolicitudes, Derivar, Aprobar, Rechazar};