import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import UsersEdit from "./views/users/edit";
import UsersView from "./views/users/show";
import UserList from "./views/users/index";
import UserAdd from "./views/users/create";

import Login from "./views/Login/Login";
import Register from "./views/Register/Register";

import CrearSolicitud from "./views/AreaVentas/vistaSolicitud";
import VerSolicitudes from "./views/AreaComercial/SolicitudesPrestamos";
import VerSolicitud from "./views/AreaComercial/VerDetalleSolicitud";
import ListaSolicitudes from "./views/AreaVentas/vistaLista";
import AnalistaDeDatosHome from "./views/AnalistaDeDatos/Home";
import Informe from "./views/AnalistaDeDatos/Informe";
import SolicitudesAceptadas from "./views/AreaComercial/SolicitudesAceptadas";
import SupervisorHome from "./views/Supervisor/SupervisorHome";

import NavBar from "./components/NavBar";

import Users from "./App";

export default function App() {
	return (
		<Router>
			<Routes>
				<Route path="/User2" element={<Users />} />

				{/* analista de datos */}
				<Route path="/analistaDeDatos/*" element={
						<NavBar tipoUsuario="Analista de Datos">
							<Routes>
								<Route path="home" element={<AnalistaDeDatosHome />} />
								<Route path="informe/:id_solicitud" element={<Informe />} />
								<Route path="/solicitud/:id_solicitud" element={<VerSolicitud />} />
							</Routes>
						</NavBar>
					}
				/>

				{/* supervisor */}
				<Route path="/supervisor/*" element={
						<NavBar tipoUsuario="Supervisor">
							<Routes>
								<Route path="solicitudes" element={<SupervisorHome />} />
								<Route path="solicitudesAceptadas" element={<SolicitudesAceptadas />}/>
								<Route path="solicitud/:id_solicitud" element={<VerSolicitud />} />
							</Routes>
						</NavBar>
					}
				/>

				{/* ejecutivo */}
				<Route path="/ejecutivo/*" element={
						<NavBar tipoUsuario="Ejecutivo">
							<Routes>
								<Route path="solicitudes" element={<VerSolicitudes />} />
								<Route path="crearSolicitud" element={<CrearSolicitud />} />
								<Route path="solicitudesAceptadas" element={<SolicitudesAceptadas />}/>
								<Route path="/solicitud/:id_solicitud" element={<VerSolicitud />} />
							</Routes>
						</NavBar>
					}
				/>

				<Route path="/lista" element={<ListaSolicitudes />} />
				<Route path="/users/create" element={<UserAdd />} />
				<Route path="/users/:id" element={<UsersView key={window.location.pathname} />}/>
				<Route path="/users/:id/edit" element={<UsersEdit />} />
				<Route path="/users" element={<UserList />} />

				<Route path="/register" element={<Register />} />
				<Route path="/login" element={<Login />} />
				<Route path="/" element={<Login />} />
			</Routes>
		</Router>
	);
}
