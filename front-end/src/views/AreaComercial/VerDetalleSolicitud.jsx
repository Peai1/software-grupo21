import React, { useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Container } from "react-bootstrap";
import axios from "axios";

import { Aprobar, Derivar } from "../../repositories/solicitud";

export default function Solicitud() {
	const { id_solicitud } = useParams();
	const tipoRol = sessionStorage.getItem("tipoUsuario");

	const [data, setData] = useState([]);

	const navigate = useNavigate();

	useEffect(() => {
		axios
			.get(`${import.meta.env.VITE_APP_API_URL}/solicitudes/${id_solicitud}`)
			.then((res) => setData(res.data))
			.catch((err) => console.error(err));
	}, [id_solicitud]);

	return (
		<body className="vistaSolicitud" style={{ margin: 0, padding: 0 }}>
			<h1
				style={{
					marginTop: "20px",
					fontSize: "30px",
					color: "white",
					marginLeft: "20px",
				}}
			>
				{" "}
				NUMERO SOLICITUD #{data.id_solicitud}
			</h1>
			<div
				className="container"
				style={{ backgroundColor: "white", padding: "20px" }}
			>
				<table className="table" style={{ backgroundColor: "white" }}>
					<tbody>
						<tr>
							<th>Rut Solicitante</th>
							<td>{data.rut_solicitante}</td>
						</tr>
						<tr>
							<th>Nombre Solicitante</th>
							<td>{data.nombre_cliente}</td>
						</tr>
						<tr>
							<th>Dirección Cliente</th>
							<td>{data.direccion_cliente}</td>
						</tr>
						<tr>
							<th>Cuotas</th>
							<td>{data.numero_cuotas}</td>
						</tr>
						<tr>
							<th>Tasa</th>
							<td>1 %</td>
						</tr>
						<tr>
							<th>Monto solicitado</th>
							<td>{data.monto_credito} CLP</td>
						</tr>
						<tr>
							<th>Valor UF</th>
							<td>{data.valor_uf_actual} CLP</td>
						</tr>
						<tr>
							<th>Valor Cuota (CLP)</th>
							<td>{data.cuota_clp} CLP</td>
						</tr>
						<tr>
							<th>Valor Total A Pagar (CLP)</th>
							<td>{data.total_clp} CLP</td>
						</tr>
						<tr>
							<th>Valor Cuota (UF)</th>
							<td>{data.cuota_en_uf} UF</td>
						</tr>
						<tr>
							<th>Valor Total A Pagar (UF)</th>
							<td>{data.total_uf} UF</td>
						</tr>
					</tbody>
				</table>
			</div>
			<Container style={{ textAlign: "center" }}>
				{tipoRol === "Ejecutivo" && data.estado === "pendiente" ? (
					<>
						<Link
							to={"/solicitudes"}
							style={{ fontSize: "26px", marginRight: "10px" }}
							onClick={() => Aprobar(id_solicitud)}
							className="mt-3 btn btn-success"
						>
							Aprobar
						</Link>

						<Link
							to={"/solicitudes"}
							style={{ fontSize: "26px", color: "white", marginRight: "10px" }}
							onClick={() => Derivar(id_solicitud)}
							className="mt-3 btn btn-warning"
						>
							Derivar
						</Link>
					</>
				) : tipoRol === "Analista de datos" ? (
					<Link
						to={`/analistaDeDatos/informe/${id_solicitud}`}
						style={{ fontSize: "26px", marginRight: "10px" }}
						className="mt-3 btn btn-primary"
					>
						Ver Informe
					</Link>
				) : null}
				<Link
					onClick={() => navigate(-1)}
					style={{ fontSize: "26px", marginRight: "10px" }}
					className="mt-3 btn btn-primary"
				>
					Volver
				</Link>
			</Container>
		</body>
	);
}
