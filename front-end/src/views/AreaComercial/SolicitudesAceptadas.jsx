import React from "react";
import "bootstrap/dist/css/bootstrap.min.css"; // Importa el CSS de Bootstrap
import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import axios from "axios";
import { useState, useEffect } from "react";

function calcularDiasHabiles(fechaInicio, fechaFin) {
	let diasHabiles = 0;
	let fechaActual = new Date(fechaInicio);

	while (fechaActual <= fechaFin) {
		// Si el día de la semana es diferente de 0 (domingo) y 6 (sábado), incrementa los días hábiles
		if (fechaActual.getDay() !== 0 && fechaActual.getDay() !== 6) {
			diasHabiles++;
		}
		fechaActual.setDate(fechaActual.getDate() + 1);
	}

	return diasHabiles;
}

function agregarDiasHabiles(fechaInicio, diasHabiles) {
    let fechaActual = new Date(fechaInicio);
    let diasAgregados = 0;

    while (diasAgregados < diasHabiles) {
        fechaActual.setDate(fechaActual.getDate() + 1);
        if (fechaActual.getDay() !== 0 && fechaActual.getDay() !== 6) {
            diasAgregados++;
        }
    }

    return fechaActual;
}

export default function SolicitudesAceptadas() {
	const [data, setData] = useState([]);
	const [error, setError] = useState(null);

	useEffect(() => {
		axios
			.get(`${import.meta.env.VITE_APP_API_URL}/solicitudes-aceptadas`)
			.then((res) => setData(res.data))
			.catch((err) => setError(err));
	}, []);

	const solicitudesVencidas = data.filter((solicitud) => {
		const fechaEnvio = new Date(solicitud.fecha_aceptacion);
		const fechaActual = new Date();
		const diasHabiles = calcularDiasHabiles(fechaEnvio, fechaActual);
		return diasHabiles > 10;
	});

	const solicitudesActivas = data.filter((solicitud) => {
		const fechaEnvio = new Date(solicitud.fecha_aceptacion);
		const fechaActual = new Date();
		const diasHabiles = calcularDiasHabiles(fechaEnvio, fechaActual);
		return diasHabiles <= 10;
	});

	if (error) return <div>Error al cargar los datos</div>;
	if (!data) return <div>Cargando...</div>;

	return (
		<body className="vistaSolicitud" style={{ margin: 0, padding: 0 }}>

			<main className="contenedorMain d-flex justify-content-center align-items-center">

            <Container
					className="pt-4 "
					style={{ margin: 40, backgroundColor: "white" }}
				>
					<div className="d-flex align-items-center">
						<h1>Solicitudes Activas</h1>
					</div>

					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Número solicitud</th>
								<th>Rut solicitante</th>
								<th>Nombre Cliente</th>
								<th>Cuotas</th>
								<th>Estado</th>
								<th>Fecha Solicitud Enviada a Cliente</th>
								<th>Días Habiles Restantes</th>
								<th>Fecha de Vencimiento</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							{solicitudesActivas.map((solicitud) => (
								<tr>
									<td>{solicitud.id_solicitud}</td>
									<td>{solicitud.rut_solicitante}</td>
									<td>{solicitud.nombre_cliente}</td>
									<td>{solicitud.numero_cuotas}</td>
									<td className="estado">{solicitud.estado}</td>
									<td>{new Date(solicitud.fecha_aceptacion).toLocaleDateString()}</td>
									<td>{10 - calcularDiasHabiles(new Date(solicitud.fecha_aceptacion),new Date())}</td>
									<td>{agregarDiasHabiles(new Date(solicitud.fecha_aceptacion), 10).toLocaleDateString()}</td> {/* Nueva celda */}

									<td>
										<Link
											to={`../solicitud/${solicitud.id_solicitud}`}
											className="btn btn-primary"
											style={{ marginRight: "10px" }}
										>
											Ver detalles
										</Link>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Container>

				<Container
					className="pt-4 "
					style={{ margin: 40, backgroundColor: "white" }}
				>
					<div className="d-flex align-items-center">
						<h1>Solicitudes Vencidas</h1>
					</div>

					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Número solicitud</th>
								<th>Rut solicitante</th>
								<th>Nombre Cliente</th>
								<th>Cuotas</th>
								<th>Estado</th>
								<th>Fecha Solicitud Enviada a Cliente</th>
								<th>Fecha Que Venció</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							{solicitudesVencidas.map((solicitud) => (
								<tr>
									<td>{solicitud.id_solicitud}</td>
									<td>{solicitud.rut_solicitante}</td>
									<td>{solicitud.nombre_cliente}</td>
									<td>{solicitud.numero_cuotas}</td>
									<td className="estado">VENCIDA</td>
									<td>{new Date(solicitud.fecha_aceptacion).toLocaleDateString()}</td>
									<td>{agregarDiasHabiles(new Date(solicitud.fecha_aceptacion), 10).toLocaleDateString()}</td>
									<td>
										<Link
											to={`../solicitud/${solicitud.id_solicitud}`}
											className="btn btn-primary"
											style={{ marginRight: "10px" }}
										>
											Ver detalles
										</Link>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Container>

			</main>
		</body>
	);
}
