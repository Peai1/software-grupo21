import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import '../../Login.css';

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
    const [attemptCount, setAttemptCount] = useState(0);

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!email || !password) {
            alert('Por favor rellena todos los campos.');
            return;
        }

        if (attemptCount >= 4) {
            alert('Has alcanzado el límite de intentos de inicio de sesión.');
            return;
        }

        try {
            const response = await axios.post('http://localhost:8080/auth/login', { email, password });
            const userType = response.data.userType;
            sessionStorage.setItem("tipoUsuario", userType);
            sessionStorage.setItem("nombre", response.data.nombre);

            switch (userType) {
                case 'Ejecutivo':
                    navigate('/ejecutivo/solicitudes');
                    break;
                case 'Analista de datos':
                    navigate('/analistaDeDatos/home');
                    break;
                case 'Supervisor':
                    // Asumiendo que tienes una ruta para los supervisores
                    navigate('/supervisor/solicitudes');
                    break;
                default:
                    navigate('/');
                    break;
            }
        } catch (error) {
            console.error('Error al iniciar sesión', error);
            setAttemptCount(attemptCount + 1);
            // Aquí puedes manejar errores, como mostrar un mensaje al usuario
        }
    };

    return(
        <div style={{background:'#142d4c'}}>
            <form>
              <div className='login'>
                <label htmlFor="username-login"> <b>Correo electrónico:</b> </label>
                <input
                    id='username-login'
                    type="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Correo electrónico"
                />
                <label htmlFor="username-password"> <b>Contraseña</b> </label>
                <input
                    id='username-password'
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Contraseña"
                />
                <button className='boton' onClick={handleSubmit} type="submit">Iniciar Sesión</button>
                <button className='boton' onClick={() => navigate('/users/create')}>Registrarse</button>
              </div>

            </form>
        </div>

    );
};

export default Login;
