import React from "react";
import { useLocation, useParams } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Sidebar from "../../components/Sidebar";
import Header from "../../components/Header";
import Container from "react-bootstrap/Container";
import '../../Login.css';

export default function Show() {
	const { id } = useParams();
	const location = useLocation();
    const query = new URLSearchParams(location.search);
    const nombre = query.get('nombre');
    const email = query.get('email');
    const userType = query.get('userType');

	
	return (
		<div>
			<Header/>
				<Container fluid className="p-0">
					<Row className="no-gutters">
						<Col xs="2">
							<Sidebar />
						</Col>
						<Col xs="10">
							<div className="container">
								<table className="table">
									<tbody>
										<tr>
											<th>ID:</th>
											<td>{id}</td>
										</tr>
										<tr>
											<th>Nombre</th>
											<td>{nombre}</td>
										</tr>
										<tr>
											<th>Email</th>
											<td>{email}</td>
										</tr>
										<tr>
											<th>Cargo</th>
											<td>{userType}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</Col>
					</Row>
				</Container>
		</div>
	);
}
