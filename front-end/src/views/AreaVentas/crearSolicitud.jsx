import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import axios from "axios";
import "../../Login.css";

const FormularioSolicitudPrestamo = () => {
	const [solicitud, setSolicitud] = useState({
		rut_solicitante: "",
		numero_cuotas: "",
		nombre_cliente: "",
		direccion_cliente: "",
		valor_uf_actual: "",
		monto_solicitado: "", // Nuevo campo para el monto solicitado
	});

	const [showModal, setShowModal] = useState(false);
	const [modalMessage, setModalMessage] = useState("");
	const [mostrarCuota, setMostrarCuota] = useState(false);

	useEffect(() => {
		const obtenerValorUF = async () => {
			const apiKey = "443c10886dde25d66d747b6be1f5aa83ff0bbb19";
			const url = `https://api.cmfchile.cl/api-sbifv3/recursos_api/uf?apikey=${apiKey}&formato=json`;

			try {
				const response = await axios.get(url);
				const valorUF = response.data.UFs[0].Valor;
				setSolicitud({ ...solicitud, valor_uf_actual: valorUF });
			} catch (error) {
				console.error("Error al obtener el valor de la UF:", error);
			}
		};

		obtenerValorUF();
	}, []);

	const handleChange = (e) => {
		// Obtén el valor ingresado
		const inputValue = e.target.value;

		setSolicitud({
			...solicitud,
			[e.target.name]: inputValue,
		});
	};

	const enviarSolicitudPrestamo = async (datosSolicitud) => {
		const url = "http://localhost:8080/crearSolicitud";
		try {
			// Asegúrate de enviar todos los campos necesarios
			const body = {
				rut_solicitante: datosSolicitud.rut_solicitante,
				numero_cuotas: datosSolicitud.numero_cuotas,
				nombre_cliente: datosSolicitud.nombre_cliente,
				direccion_cliente: datosSolicitud.direccion_cliente,
				valor_uf_actual: datosSolicitud.valor_uf_actual,
				estado: 'pendiente',
				derivada: 'false',
				monto_credito: datosSolicitud.monto_solicitado,
				cuota_en_uf: datosSolicitud.cuota_en_uf,
				total_uf: datosSolicitud.total_uf,
				cuota_clp: datosSolicitud.cuota_clp,
				total_clp: datosSolicitud.total_clp,
			};
			const response = await axios.post(url, body);
			setModalMessage("Solicitud enviada con éxito.");
			setShowModal(true);
			return response.data;
		} catch (error) {
			console.error("Error al enviar la solicitud de préstamo:", error);
			setModalMessage("Error al enviar la solicitud.");
			setShowModal(true);
			throw error;
		}
	};

	const calcularCredito = async () => {
		const valorUFcalculos = parseFloat(
			solicitud.valor_uf_actual.replace(".", "")
		);
		const tasaMensual = 0.0115;
		const valorCreditoUf = Number(solicitud.monto_solicitado) / valorUFcalculos;
		const cuotaUF =
			valorCreditoUf /
			((1 - (1 + tasaMensual) ** -Number(solicitud.numero_cuotas)) /
				tasaMensual);
		const totalUF = cuotaUF * Number(solicitud.numero_cuotas);
		setSolicitud({
			...solicitud,
			cuota_en_uf: cuotaUF.toFixed(2),
			total_uf: totalUF.toFixed(2),
			cuota_clp: (cuotaUF * valorUFcalculos).toFixed(2),
			total_clp: (totalUF * valorUFcalculos).toFixed(2),
		});
		setMostrarCuota(!mostrarCuota);
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const responseData = await enviarSolicitudPrestamo(solicitud);
			console.log("Respuesta del servidor:", responseData);
			const idSolicitud = responseData.id_solicitud; // Obtén el id de la solicitud de la respuesta del backend
			window.location.href = `solicitud/${idSolicitud}`;
		} catch (error) {
			console.error("Error al enviar el formulario:", error);
		}
	};

	const handleClose = () => {
		setShowModal(false);
		window.location.reload();
	};

	return (
		<>
			<form className="crear-solicitud" onSubmit={handleSubmit}>
				<h2>Solicitud de Prestamo</h2>
				<label>
					RUT del solicitante:
					<input
						type="text"
						name="rut_solicitante"
						placeholder="Ingrese rut"
						onChange={handleChange}
						value={solicitud.rut_solicitante}
					/>
				</label>
				<br />
				<label>
					Nombre del cliente:
					<input
						type="text"
						name="nombre_cliente"
						placeholder="Ingrese nombre"
						onChange={handleChange}
						value={solicitud.nombre_cliente}
					/>
				</label>
				<br />
				<label>
					Dirección del cliente:
					<input
						type="text"
						name="direccion_cliente"
						placeholder="Ingrese direccion"
						onChange={handleChange}
						value={solicitud.direccion_cliente}
					/>
				</label>
				<br />
				<label>
					Número de cuotas:
					<input
						type="number"
						placeholder="Numero de cuotas"
						name="numero_cuotas"
						onChange={handleChange}
						value={solicitud.numero_cuotas}
					/>
				</label>
				<br />
				<label>
					Monto Solicitado:
					<input
						type="number"
						placeholder="Monto Solicitado"
						name="monto_solicitado"
						onChange={handleChange}
						value={solicitud.monto_solicitado}
					/>
				</label>
				<br />
				<label>Valor de la UF actual: {solicitud.valor_uf_actual} CLP</label>
				<br />
				<button type="button" onClick={calcularCredito}>
					Calcular crédito
				</button>
				<br />
				{mostrarCuota && (
					<div>
						<br />
						<b>Cuota en UF:</b> {solicitud.cuota_en_uf} UF
						<br />
						<b>Total en UF:</b> {solicitud.total_uf} UF
						<br />
						<b>Cuota en CLP:</b> {solicitud.cuota_clp} CLP
						<br />
						<b>Total en CLP:</b> {solicitud.total_clp} CLP
					</div>
				)}
				<br />
				<button className="enviarSolicitud" type="submit">
					Enviar solicitud
				</button>
			</form>

			<Modal show={showModal} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Estado de la Solicitud</Modal.Title>
				</Modal.Header>
				<Modal.Body>{modalMessage}</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={handleClose}>
						Cerrar
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
};

export default FormularioSolicitudPrestamo;
