import React from "react";
import "bootstrap/dist/css/bootstrap.min.css"; // Importa el CSS de Bootstrap
import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import axios from "axios";
import { useState, useEffect } from "react";

import { Aprobar, Derivar, Rechazar } from "../../repositories/solicitud";

const clickaprobar = (a) => {
	Aprobar(a);
	window.location.reload();
};

const clickderivar = (a) => {
	Derivar(a);
	window.location.reload();
};
const clickrechazar = (a) => {
	Rechazar(a);
	window.location.reload();
};

export default function SolicitudesPrestamos() {
	const [data, setData] = useState([]);
	const [error, setError] = useState(null);

	useEffect(() => {
		axios
			.get(`http://127.0.0.1:8080/Solicitudes`)
			.then((res) => setData(res.data))
			.catch((err) => setError(err));
	}, []);

	if (error) return <div>Error al cargar los datos</div>;
	if (!data) return <div>Cargando...</div>;

	const tbody = [];

	data.forEach(
		({
			id_solicitud,
			rut_solicitante,
			numero_cuotas,
			nombre_cliente,
			fecha_actual,
			direccion_cliente,
			valor_uf_actual,
			estado,
		}) => {
			tbody.push(
				<tr>
					<td>{id_solicitud}</td>
					<td>{rut_solicitante}</td>
					<td>{nombre_cliente}</td>
					<td>{numero_cuotas}</td>
					<td className="estado">{estado}</td>
					<td>
						<Link
							to={`/solicitud/${id_solicitud}`}
							className="btn btn-primary"
							style={{ marginRight: "10px" }}
						>
							Ver detalles
						</Link>

						<Link
							to={"/solicitudes"}
							onClick={() => clickaprobar(id_solicitud)}
							className="btn btn-success"
							style={{ marginRight: "10px" }}
						>
							Aprobar
						</Link>

						<Link
							to={"/solicitudes"}
							onClick={() => clickderivar(id_solicitud)}
							className="btn btn-warning"
							style={{ marginRight: "10px" }}
						>
							Derivar
						</Link>

						<Link
							to={"/solicitudes"}
							onClick={() => clickrechazar(id_solicitud)}
							className="btn btn-danger"
						>
							Rechazar
						</Link>
					</td>
				</tr>
			);
		}
	);

	return (
		<body className="vistaSolicitud" style={{ margin: 0, padding: 0 }}>

			{/* Contenido principal */}
			<main className="contenedorMain d-flex justify-content-center align-items-center">
				<Container
					className="pt-4 "
					style={{ margin: 40, backgroundColor: "white" }}
				>
					<div className="d-flex align-items-center">
						<h1>Listado de Solicitudes</h1>
					</div>

					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Numero solicitud</th>
								<th>rut solicitante</th>
								<th>Nombre Cliente</th>
								<th>Cuotas</th>
								<th>Estado</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>{tbody}</tbody>
					</Table>
				</Container>
			</main>
		</body>
	);
}
