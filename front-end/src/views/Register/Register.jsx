import React, { useState } from 'react';
import './Register.css'; // Import the CSS file for styling

const Register = () => {
  const [nombre, setNombre] = useState('');
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("Enviando al backend:", { nombre, email, username, password });
    try {
      const response = await fetch('http://localhost:8080/auth/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ nombre, email, username, password }),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    
    <div className='box-register'> 
      <h1>Registro</h1>
      <form onSubmit={handleSubmit}>
        <div className="register"> 
          <label htmlFor="nombre">Nombre:</label>
          <input type="text" value={nombre} onChange={e => setNombre(e.target.value)} placeholder="Nombre" /> {/* Apply the same CSS class as in Login.jsx */}
          
          <label htmlFor="nombre">Correo Electrónico:</label>
          <input type="email" value={email} onChange={e => setEmail(e.target.value)} placeholder="Email" /> {/* Apply the same CSS class as in Login.jsx */} {/* Apply the same CSS class as in Login.jsx */}
          
          <label htmlFor="nombre">Contraseña:</label>
          <input type="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="Contraseña" /> {/* Apply the same CSS class as in Login.jsx */}
          
          <button type="submit" className="login-button">Registrar</button>
        </div>
      </form>
    </div>
  );
};

export default Register;
