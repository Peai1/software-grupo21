import React from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Container } from "react-bootstrap";
import { useEffect, useState } from "react";
import axios from "axios";
import { jsPDF } from "jspdf";
import html2canvas from "html2canvas";

const generatePDF = () => {
	const input = document.getElementById("informeContainer");
	html2canvas(input).then((canvas) => {
		const imgData = canvas.toDataURL("image/png");
		const pdf = new jsPDF("p", "mm", "a4");
		const imgProps = pdf.getImageProperties(imgData);
		const pdfWidth = pdf.internal.pageSize.getWidth();
		const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
		pdf.addImage(imgData, "PNG", 0, 0, pdfWidth, pdfHeight);
		const pdfBlob = pdf.output("blob");
		const url = URL.createObjectURL(pdfBlob);
		window.open(url, "_blank");
	});
};

const apiKey = import.meta.env.VITE_CMF_API_KEY;


function renderCard(title, total, cuota, error) {
	let symbol = "";
	switch (title) {
		case "Euro":
			symbol = "€";
			break;
		case "Dólar":
			symbol = "$";
			break;
		case "Pesos Chilenos":
			symbol = "CLP";
			break;
		case "UF":
			symbol = "UF";
			break;
		case "UTM":
			symbol = "UTM";
			break;
		default:
			symbol = "";
	}

	return (
		<div
			style={{
				border: "1px solid black",
				borderRadius: "10px",
				padding: "20px",
				margin: "10px",
				width: "200px",
				textAlign: "center",
			}}
		>
			<h3>{title}</h3>
			{error ? (
				<p style={{ color: "red" }}>Error al obtener el valor, intente de nuevo más tarde</p>
			) : (
				<>
				<p style={{ marginTop: "20px" }}>
				<strong>Total:</strong> {total.toFixed(2)} {symbol}
				</p>
				<p>
					<strong>Cuota:</strong> {cuota.toFixed(2)} {symbol}
				</p>
				</>
			)}
		</div>
	);
}

export default function Informe() {
	const { id_solicitud } = useParams();
	const [data, setData] = useState([]);

	useEffect(() => {
		axios
			.get(`${import.meta.env.VITE_APP_API_URL}/solicitudes/${id_solicitud}`)
			.then((res) => setData(res.data))
			.catch((err) => console.error(err));
	}, [id_solicitud]);

	const [euro, setEuro] = useState(null);
	const [dolar, setDolar] = useState(null);
	const [utm, setUtm] = useState(null);

	const [euroError, setEuroError] = useState(false);
	const [dolarError, setDolarError] = useState(false);
	const [utmError, setUtmError] = useState(false);

	useEffect(() => {
		const getLastWorkingDay = () => {
			const today = new Date();
			const day = today.getDay();
			const date = today.getDate() - (day === 0 ? 2 : day === 1 ? 3 : 1);
			return new Date(today.setDate(date));
		};

		const lastWorkingDay = getLastWorkingDay();
		const year = lastWorkingDay.getFullYear();
		const month = lastWorkingDay.getMonth() + 1; // JavaScript months are 0-based.
		const day = lastWorkingDay.getDate();

		axios
			.get(
				`https://api.cmfchile.cl/api-sbifv3/recursos_api/euro/${year}/${month}/dias/${day}?apikey=${apiKey}&formato=json`
			)
			.then((response) => {
				setEuro(response.data.Euros[0].Valor);
			})
			.catch((error) => {
				console.error("Error obteniendo el valor del Euro:", error);
				setEuroError(true);
			});

		axios
			.get(
				`https://api.cmfchile.cl/api-sbifv3/recursos_api/dolar/${year}/${month}/dias/${day}?apikey=${apiKey}&formato=json`
			)
			.then((response) => {
				setDolar(response.data.Dolares[0].Valor);
			})
			.catch((error) => {
				console.error("Error obteniendo el valor del Dólar:", error);
				setDolarError(true);
			});

		axios
			.get(
				`https://api.cmfchile.cl/api-sbifv3/recursos_api/utm/${year}/${month}/dias/${day}?apikey=${apiKey}&formato=json`
			)
			.then((response) => {
				setUtm(response.data.UTMs[0].Valor);
			})
			.catch((error) => {
				console.error("Error obteniendo el valor de la UTM:", error);
				setUtmError(true);
			});
	}, []);

	return (
		<div className="vistaSolicitud" style={{ margin: 0, padding: 0 }}>
			{/* <header>
				<AnalistaDeDatosNavBar />
			</header> */}

			<h1
				style={{
					marginTop: "20px",
					fontSize: "30px",
					color: "white",
					marginLeft: "20px",
				}}
			>
				{" "}
				NÚMERO SOLICITUD #{data.id_solicitud}
			</h1>

			<Container
				id="informeContainer"
				style={{
					backgroundColor: "white",
					padding: "20px",
					borderRadius: "15px",
					marginTop: "40px",
					width: "60%",
				}}
			>
				<h1
					style={{
						backgroundColor: "#007bff",
						color: "white",
						padding: "10px",
						borderRadius: "0px",
						textAlign: "center",
						fontSize: "24px",
						marginBottom: "0px",
					}}
				>
					Financiera La Clave - NÚMERO SOLICITUD #{data.id_solicitud}
				</h1>

				<h2
					style={{
						backgroundColor: "#007bff",
						color: "white",
						padding: "10px",
						borderRadius: "0px",
						marginBottom: "20px",
						textAlign: "center",
					}}
				>
					Informe de Crédito
				</h2>
				<div
					style={{
						color: "black",
						fontSize: "18px",
						lineHeight: "2",
						marginLeft: "30px",
					}}
				>
					<strong>Nombre del Solicitante:</strong> {data.nombre_cliente}
					<br />
					<strong>RUT del Solicitante:</strong> {data.rut_solicitante}
					<br />
					<strong>Monto Solicitado:</strong> {data.monto_credito} CLP
					<br />
					<strong>Tasa:</strong> 1 %<br />
					<strong>Número de Cuotas:</strong> {data.numero_cuotas}
					<br />
					<strong>Valor UF Utilizado:</strong> {data.valor_uf_actual} CLP
					<br />
				</div>

				<div
					style={{
						display: "flex",
						justifyContent: "center",
						marginTop: "20px",
					}}
				>
					{renderCard(
						"Pesos Chilenos",
						parseFloat(data.total_clp),
						data.total_clp / data.numero_cuotas
					)}
					{renderCard(
						"UF",
						data.total_clp / data.valor_uf_actual,
						data.total_clp / data.valor_uf_actual / data.numero_cuotas
					)}
					{renderCard(
						"UTM",
						data.total_clp / parseFloat(utm),
						data.total_clp / parseFloat(utm) / data.numero_cuotas,
						utmError
					)}
				</div>
				<div
					style={{
						display: "flex",
						justifyContent: "center",
					}}
				>
					{renderCard(
						"Euro",
						data.total_clp / parseFloat(euro),
						data.total_clp / parseFloat(euro) / data.numero_cuotas,
						euroError
					)}
					{renderCard(
						"Dólar",
						data.total_clp / parseFloat(dolar),
						data.total_clp / parseFloat(dolar) / data.numero_cuotas,
						dolarError
					)}
				</div>

				<div
					style={{
						backgroundColor: "#007bff",
						color: "white",
						padding: "10px",
						borderRadius: "5px",
						marginTop: "20px",
						textAlign: "center",
						fontSize: "16px",
					}}
				>
					Gracias por elegir Financiera La Clave
				</div>
			</Container>

			<div
				style={{
					marginTop: "20px",
					display: "flex",
					justifyContent: "center",
					paddingBottom: "50px",
				}}
			>
				<Link
					to={`/analistaDeDatos/solicitud/${id_solicitud}`}
					style={{
						display: "inline-block",
						backgroundColor: "#007bff",
						color: "white",
						padding: "10px 30px",
						borderRadius: "5px",
						textDecoration: "none",
						fontSize: "20px",
					}}
				>
					Volver
				</Link>

				<button
					style={{
						display: "inline-block",
						backgroundColor: "#007bff",
						color: "white",
						padding: "10px 30px",
						borderRadius: "5px",
						textDecoration: "none",
						fontSize: "20px",
						marginLeft: "20px",
					}}
					onClick={generatePDF}
				>
					Generar PDF
				</button>
			</div>
		</div>
	);
}
