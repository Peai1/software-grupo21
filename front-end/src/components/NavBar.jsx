import React from "react";
import { Navbar } from "react-bootstrap";
import "../Login.css";
import { Link } from "react-router-dom";

const CustomNavBar = ({ tipoUsuario, children }) => {
	return (
		<div>
		<Navbar className="navbarVentas" expand="lg" style={{ padding: "15px" }}>
			<Navbar.Brand href="#home">
				<h2 className="brand">{tipoUsuario}: {sessionStorage.getItem("nombre")}</h2>
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse
				id="basic-navbar-nav"
				className="justify-content-between"
			>
				{tipoUsuario === "Analista de Datos" && (
					<Link to={`/analistaDeDatos/home`} className="btn btn-primary mr-3">
						Ver todas las solicitudes
					</Link>
				)}

				{tipoUsuario === "Supervisor" && (
					<div className="d-flex justify-content-start">
						<Link to={`/supervisor/solicitudes`} className="btn btn-primary mr-3">
							Ver todas las solicitudes
						</Link>
						<Link
							to={`/supervisor/solicitudesAceptadas`}
							className="btn btn-primary"
							style={{ marginLeft: "10px" }}
						>
							Ver Solicitudes Ingresadas
						</Link>
					</div>
				)}

				{tipoUsuario === "Ejecutivo" && (
					<div className="d-flex justify-content-start">
						<Link
							to={`/ejecutivo/crearSolicitud`}
							className="btn btn-primary"
							style={{ marginRight: "10px" }}
						>
							Crear Solicitud
						</Link>
						<Link to={`/ejecutivo/solicitudes`} className="btn btn-primary">
							Ver todas las solicitudes
						</Link>
						<Link
							to={`/ejecutivo/solicitudesAceptadas`}
							className="btn btn-primary"
							style={{ marginLeft: "10px" }}
						>
							Ver Solicitudes Ingresadas
						</Link>
					</div>
				)}

				<Link to={"/Login"} className="btn btn-danger ml-auto">
					Cerrar Sesión
				</Link>
			</Navbar.Collapse>
		</Navbar>
		{children}
		</div>
	);
};

export default CustomNavBar;
