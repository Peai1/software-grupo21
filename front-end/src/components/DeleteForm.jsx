import React from "react";
import PropTypes from "prop-types"; // Importamos PropTypes para validar las propiedades
import { mutate } from 'swr';

export default function DeleteForm({ id, callback }) {
	const deleteUser = async (e) => { // Función asincrónica para borrar el usuario
		e.preventDefault(); // Prevenimos el comportamiento por defecto del formulario
		try {
			await callback(id); // Llamamos a la función de callback pasando el id del usuario
			mutate("/users/all"); // Actualizamos la cache de SWR
			alert("Elemento recargado correctamente"); // Mostramos un mensaje de éxito
		} catch (error) {
			alert("A ocurrido un error al borrar el elemento"); // Mostramos un mensaje de error
		}
	};
	return (
		<form onSubmit={deleteUser} className="d-inline-block ml-4" action="">
			<input type="hidden" name="id" value={id} />
			<button onClick={deleteUser} className="btn btn-danger" type="button">
				Borrar
			</button>
		</form>
	);
}
DeleteForm.propTypes = {
	id: PropTypes.number.isRequired, // Propiedad id de tipo número requerida
	callback: PropTypes.func.isRequired, // Propiedad callback de tipo función requerida
};
