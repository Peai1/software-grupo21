import { Sequelize } from 'sequelize';
import sequelize from '../database.js';

class User extends Sequelize.Model {};

User.init({
  idUsuario: {
    type: Sequelize.DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  nombre: Sequelize.DataTypes.STRING,
  email: {
    type: Sequelize.DataTypes.STRING,
    unique: true,
    allowNull: false
  },
  userType: {
    type: Sequelize.DataTypes.STRING,
    validate: {
      isIn: [['Gerente', 'Analista de datos', 'Supervisor', 'Ejecutivo']]
    }
  },
  password: {
    type: Sequelize.DataTypes.STRING,
    allowNull: false
  }},
  {
    sequelize,
    timestamps: true,
  }
);


export default User;
