import UserController from './UserController.js';
import { crearSolicitud } from './solicitudPrestamoController.js';
import { obtenerTodasLasSolicitudes } from './solicitudPrestamoController.js';
import { getAll, get, aprobar, derivar, rechazar, eliminarSolicitud, obtenerSolicitudesAceptadas } from './solicitudPrestamoController.js';
import AuthController from './authController.js';

export default (app) => {
	const userController = new UserController();
	const authController = new AuthController();

	// Login y Registro
	app.post('/auth/login', authController.login);
	app.post('/createUser', authController.register);

	// Usuarios
	app.get('/users', userController.getAll);
	app.get('/users/:userId', userController.get);
	app.put('/users/:userId', userController.update);
	app.delete('/users/:userId', userController.delete);
	app.post('/crearSolicitud', crearSolicitud);
	app.get('/solicitudes-ventas', obtenerTodasLasSolicitudes);

	// Solicitudes
	app.get('/solicitudes',getAll);
	app.get('/solicitudes/:id_solicitud',get);
	app.put('/solicitudes/:id_solicitud/aprobar',aprobar);
	app.put('/solicitudes/:id_solicitud/derivar',derivar);
	app.put('/solicitudes/:id_solicitud/rechazar',rechazar);
	app.delete('/solicitudes/:id_solicitud', eliminarSolicitud);

	app.get('/solicitudes-aceptadas', obtenerSolicitudesAceptadas);	

};