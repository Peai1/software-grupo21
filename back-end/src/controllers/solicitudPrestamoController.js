import SolicitudPrestamo from '../models/SolicitudPrestamo.js';

const crearSolicitud = async (req, res) => {
    try {
        const {
            rut_solicitante,
            numero_cuotas,
            nombre_cliente,
            direccion_cliente,
            valor_uf_actual,
            estado,
            monto_credito,
            derivada,
            cuota_en_uf,
            total_uf,
            cuota_clp,
            total_clp,
        } = req.body;

        // Verificar si faltan campos requeridos
        if (!rut_solicitante || !numero_cuotas || !nombre_cliente || !direccion_cliente || !valor_uf_actual || !estado || !monto_credito || !derivada || !cuota_en_uf || !total_uf || !cuota_clp || !total_clp) {
            return res.status(400).json({ error: "Faltan campos requeridos" });
        }

        // Verificar si los campos numéricos son realmente números
        if (isNaN(numero_cuotas) || isNaN(monto_credito) || isNaN(cuota_en_uf) || isNaN(total_uf) || isNaN(cuota_clp) || isNaN(total_clp)) {
            return res.status(400).json({ error: "Uno o más campos numéricos no son números" });
        }

        // Reemplazar comas por puntos y eliminar puntos de mil
        const valorUF = parseFloat(valor_uf_actual.replace(/\./g, '').replace(/,/g, '.'));

        // Convierte los campos monto_credito y cuota_en_uf a números en caso de ser necesario
        const montoCredito = parseFloat(monto_credito);
        const cuotaUF = parseFloat(cuota_en_uf)
        const totalUF = parseFloat(total_uf)
        const cuotaCLP = parseFloat(cuota_clp)
        const totalCLP = parseFloat(total_clp)

        // Crear una nueva solicitud de préstamo
        const nuevaSolicitud = await SolicitudPrestamo.create({
            rut_solicitante,
            numero_cuotas,
            nombre_cliente,
            direccion_cliente,
            valor_uf_actual: valorUF,
            estado,
            monto_credito: montoCredito,
            derivada: derivada === 'true', // Convierte a booleano
            cuota_en_uf: cuotaUF,
            total_uf: totalUF,
            cuota_clp: cuotaCLP,
            total_clp: totalCLP,
            fecha_actual: new Date(),
            fecha_aceptacion: null
        });

        res.status(201).json(nuevaSolicitud);
    } catch (error) {
        console.error(error); // Imprimir el error en la consola para depuración
        res.status(400).json({ error: error.message });
    }
};

const obtenerTodasLasSolicitudes = async (req, res) => {
    try {
        // Ahora puedes usar el modelo para obtener todas las solicitudes
        const solicitudes = await SolicitudPrestamo.findAll();

        res.json(solicitudes);
    } catch (error) {
        console.error('Error al obtener las solicitudes:', error);
        res.status(500).json({ error: 'Error al obtener las solicitudes' });
    }
};


const getAll = async (req, res) => {
    // Obtener todas las solicitudes pendientes y no derivadas
    const solicitudes = await SolicitudPrestamo.findAll({
        where: {
            estado: "pendiente",
            derivada: "False"
        }
    });

    res.send(solicitudes);
}

const get = async (req, res) => {
    try {
        if (isNaN(req.params.id_solicitud)) {
            return res.status(400).json({ error: "id_solicitud debe ser un número" });
        }

        // Obtener una solicitud por su ID
        const solicitud = await SolicitudPrestamo.findByPk(req.params.id_solicitud);

        if (!solicitud) {
            return res.status(404).json({ error: "Solicitud no encontrada" });
        }

        res.send(solicitud);
    } catch (error) {
        console.error(error); // Imprimir el error en la consola para depuración
        res.status(400).json({ error: error.message });
    }
}


const aprobar = async (req, res) => {
    try {
        // Actualizar el estado de la solicitud a 'Aceptada'
        await SolicitudPrestamo.update({ estado: 'aceptada', fecha_aceptacion: new Date() }, {
            where: { id_solicitud: req.params.id_solicitud }
        });

        res.json({ msg: 'Solicitud aprobada' });
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

const rechazar = async (req, res) => {
    const solicitud = await SolicitudPrestamo.findByPk(req.params.id_solicitud);
    solicitud.update({ estado: "rechazada" });
    res.send(solicitud);
}

const derivar = async (req, res) => {
    const solicitud = await SolicitudPrestamo.findByPk(req.params.id_solicitud);
    solicitud.update({ derivada: "True" });
    res.send(solicitud);
}

const eliminarSolicitud = async (req, res) => {
    const solicitud = await SolicitudPrestamo.findByPk(req.params.id_solicitud);

    if (!solicitud) {
        return res.status(404).json({ error: "Solicitud no encontrada" });
    }

    solicitud.destroy();
    res.send(solicitud);
}

const obtenerSolicitudesAceptadas = async (req, res) => {
    // Obtener todas las solicitudes aceptadas
    const solicitudes = await SolicitudPrestamo.findAll({
        where: {
            estado: "aceptada"
        }
    });

    res.send(solicitudes);
}

export { crearSolicitud, getAll, get, aprobar,rechazar, derivar, obtenerTodasLasSolicitudes, eliminarSolicitud, obtenerSolicitudesAceptadas};


