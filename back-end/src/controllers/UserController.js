import User from "../models/User.js";

export default class UserController {
  // Método para obtener todos los usuarios
  async getAll(req, res) {
    const users = await User.findAll();
    res.send(users);
  }

  // Método para obtener usuarios por nombre
  async getBynombre(req, res) {
    const users = await User.findAll({
      where: {
        nombre: req.params.nombre,
      },
    });
    res.send(users);
  }

  // Método para obtener un usuario por su ID
  async get(req, res) {
    const user = await User.findByPk(req.params.id);
    res.send(user);
  }

  // Método para actualizar un usuario
  async update(req, res) {
    try {
      const user = await User.findByPk(req.params.userId);
      await user.update({ nombre: req.body.nombre, email: req.body.email });
      res.send(user);
    } catch (error) {
      res.status(500).send({ message: "Error updating user" });
    }
  }

  // Método para eliminar un usuario
  async delete(req, res) {
    try {
      await User.destroy({ where: { id: req.params.userId } });
      res.send({ status: "ok" });
    } catch (error) {
      res.status(500).send({ message: "Error deleting user" });
    }
  }
}
