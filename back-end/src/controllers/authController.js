import User from "../models/User.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export default class AuthController {

  async register(req, res) {
    try {
      const data = {
        nombre: req.body.nombre, // Nombre del usuario obtenido del cuerpo de la solicitud
        email: req.body.email, // Email del usuario obtenido del cuerpo de la solicitud
        password: req.body.password, // Contraseña del usuario obtenida del cuerpo de la solicitud
      };

      if (!data.nombre || !data.email || !data.password) {
        return res.status(400).json({ message: "Por favor rellena todos los campos." });
      }

      if (data.password.length < 8) {
        return res.status(400).json({ message: "La contraseña debe tener al menos 8 caracteres." });
      }

      const user = await User.findOne({
        where: { email: data.email }, // Buscar si ya existe un usuario con el mismo email
      });
      
      if (user) {
        return res.status(409).json({ message: "Usuario ya existente" }); // Si el usuario ya existe, devolver un mensaje de error
      }

      const encryptPass = await bcrypt.hash(data.password, 13); // Encriptar la contraseña del usuario

      const resp = await User.create({
        nombre: req.body.nombre, // Nombre del usuario obtenido del cuerpo de la solicitud
        email: req.body.email, // Email del usuario obtenido del cuerpo de la solicitud
        password: encryptPass, // Contraseña encriptada del usuario
        userType: req.body.userType, // Tipo de usuario obtenido del cuerpo de la solicitud
      });

      res.status(200).json({
        id: resp.idUsuario, // Devolver el ID del usuario creado
      });

    } catch (error) {
      res.status(500).send({ message: "Error al crear usuario" }); // Si ocurre un error, devolver un mensaje de error
    }
  }

  async login(req, res) {
    const { email, password } = req.body; // Obtener el email y la contraseña del cuerpo de la solicitud
    const user = await User.findOne({
      where: {
        email: email, // Buscar un usuario con el email proporcionado
      },
    });

    if (!user) {
      return res.status(404).send({ message: "Usuario no encontrado" }); // Si no se encuentra un usuario, devolver un mensaje de error
    }

    const match = await bcrypt.compare(password, user.password); // Comparar la contraseña proporcionada con la contraseña almacenada en la base de datos

    if (!match) {
      return res.status(401).json({ message: "Credenciales incorrectas" });
    }
    else {
      const tokenPayload = {
        id_usuario: user.idUsuario, // ID del usuario para el payload del token
      };

      const tiempoExpiracion = 3000; // Tiempo en segundos
      const token = jwt.sign(tokenPayload, process.env.JWT_KEY, { expiresIn: tiempoExpiracion }); // Generar un token JWT con el payload y la clave secreta

      res.cookie("token", token, {
        maxAge: tiempoExpiracion * 1000, // Establecer la duración máxima del token en milisegundos
        httpOnly: true, // Hacer que la cookie solo sea accesible a través de HTTP
        sameSite: "None", // Permitir que la cookie se envíe en solicitudes de sitios cruzados
        secure: true, // Hacer que la cookie solo se envíe a través de conexiones seguras (HTTPS)
      });

      res.status(200).json({ userType: user.userType, nombre: user.nombre }); // Devolver el tipo de usuario y el nombre del usuario
    }
    else {
      res.status(401).send({ message: "Credenciales incorrectas" }); // Si las credenciales son incorrectas, devolver un mensaje de error
    }
  }
}
